class AddTeams < Mongo::Migration
  @bundesliga = League.where(name: 'Bundesliga').first.id
  @SerieA = League.where(name: 'Serie A').first.id
  @Ligue1 = League.where(name: 'Ligue 1').first.id
  @Eredivisie = League.where(name: 'Eredivisie').first.id
  @LaLiga =  League.where(name: 'La Liga').first.id

  def self.up
    insert :teams, name: 'FSV Mainz 05', league_id: @bundesliga
    insert :teams, name: 'Bayer Leverkusen', league_id: @bundesliga
    insert :teams, name: 'Bayern Munich', league_id: @bundesliga
    insert :teams, name: 'B. Monchengladbach', league_id: @bundesliga
    insert :teams, name: 'Darmstadt', league_id: @bundesliga
    insert :teams, name: 'Dortmund', league_id: @bundesliga
    insert :teams, name: 'Eintracht Frankfurt', league_id: @bundesliga
    insert :teams, name: 'FC Augsburg', league_id: @bundesliga
    insert :teams, name: 'FC Koln', league_id: @bundesliga
    insert :teams, name: 'Hamburger SV', league_id: @bundesliga
    insert :teams, name: 'Hertha Berlin', league_id: @bundesliga
    insert :teams, name: 'Hoffenheim', league_id: @bundesliga
    insert :teams, name: 'Ingolstadt', league_id: @bundesliga
    insert :teams, name: 'RB Leipzig', league_id: @bundesliga
    insert :teams, name: 'SC Freiburg', league_id: @bundesliga
    insert :teams, name: 'Schalke', league_id: @bundesliga
    insert :teams, name: 'SV Werder Bremen', league_id: @bundesliga
    insert :teams, name: 'Wolfsburg', league_id: @bundesliga

    insert :teams, name: 'Angers', league_id: @SerieA
    insert :teams, name: 'Bastia', league_id: @SerieA
    insert :teams, name: 'Bordeaux', league_id: @SerieA
    insert :teams, name: 'Caen', league_id: @SerieA
    insert :teams, name: 'Dijon', league_id: @SerieA
    insert :teams, name: 'Guingamp', league_id: @SerieA
    insert :teams, name: 'Lille', league_id: @SerieA
    insert :teams, name: 'Lorient', league_id: @SerieA
    insert :teams, name: 'Lyon', league_id: @SerieA
    insert :teams, name: 'Metz', league_id: @SerieA
    insert :teams, name: 'Monaco', league_id: @SerieA
    insert :teams, name: 'Montpellier', league_id: @SerieA
    insert :teams, name: 'Nancy', league_id: @SerieA
    insert :teams, name: 'Nantes', league_id: @SerieA
    insert :teams, name: 'Nice', league_id: @SerieA
    insert :teams, name: 'Paris SG', league_id: @SerieA
    insert :teams, name: 'Rennes', league_id: @SerieA
    insert :teams, name: 'St Etienne', league_id: @SerieA
    insert :teams, name: 'Toulouse', league_id: @SerieA

    insert :teams, name: 'AC Milan', league_id: @Ligue1
    insert :teams, name: 'AS Roma', league_id: @Ligue1
    insert :teams, name: 'Atalanta', league_id: @Ligue1
    insert :teams, name: 'Bologna', league_id: @Ligue1
    insert :teams, name: 'Cagliari', league_id: @Ligue1
    insert :teams, name: 'Chievo', league_id: @Ligue1
    insert :teams, name: 'Crotone', league_id: @Ligue1
    insert :teams, name: 'Empoli', league_id: @Ligue1
    insert :teams, name: 'Fiorentina', league_id: @Ligue1
    insert :teams, name: 'Genoa', league_id: @Ligue1
    insert :teams, name: 'Inter', league_id: @Ligue1
    insert :teams, name: 'Juventus', league_id: @Ligue1
    insert :teams, name: 'Lazio', league_id: @Ligue1
    insert :teams, name: 'Napoli', league_id: @Ligue1
    insert :teams, name: 'Palermo', league_id: @Ligue1
    insert :teams, name: 'Pescara', league_id: @Ligue1
    insert :teams, name: 'Sampdoria', league_id: @Ligue1
    insert :teams, name: 'Sassuolo', league_id: @Ligue1
    insert :teams, name: 'Torino', league_id: @Ligue1
    insert :teams, name: 'Udinese', league_id: @Ligue1

    insert :teams, name: 'Ajax', league_id: @Eredivisie
    insert :teams, name: 'AZ Alkmaar', league_id: @Eredivisie
    insert :teams, name: 'Den Haag', league_id: @Eredivisie
    insert :teams, name: 'Excelsior', league_id: @Eredivisie
    insert :teams, name: 'Feyenoord', league_id: @Eredivisie
    insert :teams, name: 'G.A. Eagles', league_id: @Eredivisie
    insert :teams, name: 'Groningen', league_id: @Eredivisie
    insert :teams, name: 'Heerenveen', league_id: @Eredivisie
    insert :teams, name: 'Heracles', league_id: @Eredivisie
    insert :teams, name: 'Nijmegen', league_id: @Eredivisie
    insert :teams, name: 'PSV', league_id: @Eredivisie
    insert :teams, name: 'Sparta Rotterdam', league_id: @Eredivisie
    insert :teams, name: 'Twente', league_id: @Eredivisie
    insert :teams, name: 'Utrecht', league_id: @Eredivisie
    insert :teams, name: 'Vitesse', league_id: @Eredivisie
    insert :teams, name: 'Willem II', league_id: @Eredivisie
    insert :teams, name: 'Zwolle', league_id: @Eredivisie
    insert :teams, name: 'Zwolle', league_id: @Eredivisie

    insert :teams, name: 'Alaves', league_id: @LaLiga
    insert :teams, name: 'Ath Bilbao', league_id: @LaLiga
    insert :teams, name: 'Atl. Madrid', league_id: @LaLiga
    insert :teams, name: 'Barcelona', league_id: @LaLiga
    insert :teams, name: 'Betis', league_id: @LaLiga
    insert :teams, name: 'Celta Vigo', league_id: @LaLiga
    insert :teams, name: 'Dep. La Coruna', league_id: @LaLiga
    insert :teams, name: 'Eibar', league_id: @LaLiga
    insert :teams, name: 'Espanyol', league_id: @LaLiga
    insert :teams, name: 'Gijon', league_id: @LaLiga
    insert :teams, name: 'Granada CF', league_id: @LaLiga
    insert :teams, name: 'Las Palmas', league_id: @LaLiga
    insert :teams, name: 'Leganes', league_id: @LaLiga
    insert :teams, name: 'Malaga', league_id: @LaLiga
    insert :teams, name: 'Osasuna', league_id: @LaLiga
    insert :teams, name: 'Real Madrid', league_id: @LaLiga
    insert :teams, name: 'Real Sociedad', league_id: @LaLiga
    insert :teams, name: 'Sevilla', league_id: @LaLiga
    insert :teams, name: 'Valencia', league_id: @LaLiga
    insert :teams, name: 'Villarreal', league_id: @LaLiga
  end
end