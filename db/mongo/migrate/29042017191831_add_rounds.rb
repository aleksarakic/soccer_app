class AddRounds < Mongo::Migration

  def self.up
    insert :rounds, number: 1, team_ids: [Team.first.id, Team.all[1].id]
  end
end