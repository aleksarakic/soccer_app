class AddSeasons < Mongo::Migration
  def self.up
    insert :seasons, year: 2017, league_id: League.where(name: 'Bundesliga').first.id
  end
end