class AddCountries < Mongo::Migration
  def self.up
    insert :countries, name: 'Italy', alpha2_code: 'IT', alpha3_code: 'ITA'
    insert :countries, name: 'France', alpha2_code: 'FR', alpha3_code: 'FRA'
    insert :countries, name: 'Germany', alpha2_code: 'DE', alpha3_code: 'DEU'
    insert :countries, name: 'England', alpha2_code: 'FR', alpha3_code: 'FRA'
    insert :countries, name: 'Spain', alpha2_code: 'ES', alpha3_code: 'ESP'
    insert :countries, name: 'Netherlands', alpha2_code: 'NL', alpha3_code: 'NLD'
  end
end