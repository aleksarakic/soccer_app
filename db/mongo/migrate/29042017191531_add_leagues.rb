class AddLeagues < Mongo::Migration
  def self.up
    insert :leagues, name: 'Serie A', country_id: Country.where(alpha2_code: 'IT').first.id
    insert :leagues, name: 'Ligue 1', country_id: Country.where(alpha2_code: 'FR').first.id
    insert :leagues, name: 'Bundesliga', country_id: Country.where(alpha2_code: 'DE').first.id
    insert :leagues, name: 'Eredivisie', country_id: Country.where(alpha2_code: 'NL').first.id
    insert :leagues, name: 'La Liga', country_id: Country.where(alpha2_code: 'ES').first.id
  end
end