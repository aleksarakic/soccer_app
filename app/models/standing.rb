class Standing # rename to Results?
  include Mongoid::Document
  include Mongoid::Timestamps

  field :points, type: Integer, default: 0

  belongs_to :round
  belongs_to :team
  belongs_to :season

  class << self
    def prints hash
      hash.each_with_index do |(team, points), index|
        puts "#{index+1}: #{team} | points: #{points}"
      end
    end

    def sort hash
      hash.sort_by {|key, value| value }.reverse
    end

    def league_season_round league_name, season_year, round_number
      league = League.where(name: league_name).first
      season = Season.where(year: season_year).first
      round = Round.where(number: round_number).first
      teams = Team.where(league_id: league.id, season_ids: season.id)

      team_hash = {}

      teams.each do |t|
        standing = Standing.in(round: round, team: t).last
        team_hash[t.name] = standing.points
      end

      Standing.prints(Standing.sort team_hash)
    end

    def specific_league_current_standing league, season
      league_id = League.where(name: league).first.id
      hash = {}
      all_teams = Team.where(league_id: league_id)
      teams_with_result = Team.in(league_id: league_id, season_ids: Season.where(year: season).first.id)
      def_teams = all_teams - teams_with_result + teams_with_result

      def_teams.each do |team|
        hash[team.name] = team.points
      end

      Standing.prints(Standing.sort hash)
    end

    def season_round_team season_year, round_number, team_name
      team = Team.in(name: team_name, season_ids: Season.where(year: season_year).first).first
      standing = Standing.in(round: Round.where(number: round_number).first, team: team).last

      puts hash = {team.name => standing.points}
      Standing.prints(Standing.sort hash)
    end
  end
end
