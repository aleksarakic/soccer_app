class League
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String

  belongs_to :country
  has_many :teams
  has_many :seasons # kako odrediti season

  def current_standing season
    Standing.specific_league_current_standing(name, season)
  end
end