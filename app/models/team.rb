class Team
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :matches_played, type: Integer, default: 0
  field :wins, type: Integer, default: 0
  field :loses, type: Integer, default: 0
  field :draws, type: Integer, default: 0
  field :goals, type: Integer, default: 0
  field :points, type: Integer, default: 0

  has_many :standings
  has_many :games, class_name: 'Team'
  has_and_belongs_to_many :rounds
  has_and_belongs_to_many :seasons
  belongs_to :league
end
