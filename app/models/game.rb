class Game
  include Mongoid::Document
  include Mongoid::Timestamps

  field :location, type: String
  field :home_team_goals, type: Integer
  field :away_team_goals, type: Integer

  belongs_to :home_team, class_name: "Team"
  belongs_to :away_team, class_name: "Team"
  belongs_to :round
  belongs_to :season

  has_many :goals

  validates_presence_of :home_team, :away_team

  after_initialize :setting_goals, :setting_season_on_teams, :which_round
  before_save :setting_points, :init_standing, :teams_same_league?

  def teams_same_league?
    return unless self.new_record?
    unless teams[0].league_id == teams[1].league_id
      raise 'Teams have to be of the same League'
    end
  end

  def random_player
    ['aleksa', 'dragan', 'dragoslav'].sample
  end

  def random_minute
    (0..90).to_a.sample
  end

  def creating_goals minute, player, team
    Goal.create(minute: minute, player: player, team: team, game: self)
  end

  def setting_goals
    return unless self.new_record?
    home_team_goals.times {|x| creating_goals(random_minute, random_player, home_team) }
    away_team_goals.times {|x| creating_goals(random_minute, random_player, away_team) }
  end

  def teams
    [home_team, away_team]
  end

  def setting_teams
    return unless self.new_record?

    self.update_attributes(teams: [Team.find(home_team.id), Team.find(away_team.id)])
  end

  def setting_season_on_teams
    return unless self.new_record?
    teams.map {|team| team.update_attributes(season_ids: [season.id])}
  end

  def init_standing
    return unless self.new_record?

    teams.each do |team|
      Standing.create(round_id: self.round_id, team_id: team.id, points: team.points, season: self.season)
    end
  end

  def which_round
    return unless self.new_record?

    last_round = Round.last

    if last_round.games_arr.length < 4
      self.update_attributes(round_id: last_round.id)
      last_round.games_arr << self.id
      last_round.save!
    else
      # distinct teams
      round = Round.new(number: last_round.number + 1, team_ids: [home_team.id, away_team.id])
      self.round_id = round.id
      round.update_attributes(games_arr: [self.id])
      round.save!
    end
  end

  def setting_points
    return unless self.new_record?

    home_team.matches_played += 1
    away_team.matches_played += 1

    if home_team_goals > away_team_goals
      home_team.wins += 1
      away_team.loses += 1
      home_team.points += 3
    elsif away_team_goals > home_team_goals
      away_team.wins += 1
      home_team.loses += 1
      away_team.points += 3
    elsif away_team_goals == home_team_goals
      home_team.draws += 1
      away_team.draws += 1
      home_team.points += 1
      away_team.points += 1
    end
  end

  def result
    "#{goals.where(team: home_team).count}:#{goals.where(team: away_team).count}"
  end

  def first_half_result
    puts "#{goals.where(team: home_team, :minute.lte => 45).count}:#{goals.where(team: away_team, :minute.lt => 45).count}"
    puts goals.where(:minute.lt => 45).each {|goal| p "#{goal.player}: " + "#{goal.minute}"};nil
  end

  def second_half_result
    puts "#{goals.where(team: home_team).count}:#{goals.where(team: away_team).count}"
    puts goals.where(:minute.gt => 45).sort.each {|goal| p "#{goal.player}: " + "#{goal.minute}"};nil
  end
end
