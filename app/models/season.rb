class Season
  include Mongoid::Document
  include Mongoid::Timestamps

  field :year, type: Integer

  has_and_belongs_to_many :teams
  has_many :standings
  belongs_to :league
  has_many :games
end
