class Country
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :alpha2_code, type: String
  field :alpha3_code, type: String

  has_many :leagues
end
