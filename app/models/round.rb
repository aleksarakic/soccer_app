class Round
  include Mongoid::Document
  include Mongoid::Timestamps

  field :number, type: Integer
  field :games_arr, type: Array, default: []

  has_one :standing
  has_and_belongs_to_many :teams

  validates_presence_of :number

end
