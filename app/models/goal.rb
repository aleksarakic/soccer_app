class Goal
  include Mongoid::Document
  include Mongoid::Timestamps

  field :minute, type: Integer
  field :player, type: String

  belongs_to :game
  belongs_to :team
end
