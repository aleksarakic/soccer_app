namespace :db do
  namespace :mongo do

    desc 'Drops all the collections for the database for the current Rails.env'
    task :drop do
      Mongoid.master.collections.each { |col| col.drop_indexes && col.drop unless ['system.indexes', 'system.users'].include?(col.name) }
    end

    desc 'Load the seed data from db/mongo/seeds.rb'
    task :seed do
      seed_file = File.join(Rails.application.root, 'db', 'mongo', 'seeds.rb')
      load(seed_file) if File.exist?(seed_file)
    end

    desc 'Create the database, and initialize with the seed data'
    task :setup => %w(db:mongo:create db:mongo:seed)

    desc 'Delete data and seed'
    task :reseed => %w(db:mongo:drop db:mongo:seed)

    task :create do
      puts 'Creating MONGODB'
    end

    desc 'Current MongoDB database version'
    task :version do
      puts Mongo::Migrator.current_version.to_s
    end

    task :all_versions do
      puts Mongo::Migrator.all_versions
    end

    desc 'Migrate MongoDB database through scripts in db/mongo/migrate. Target specific version with VERSION=x.'
    task :migrate do
      Mongo::Migration.verbose = ENV['VERBOSE'] ? ENV['VERBOSE'] == 'true' : true
      Mongo::Migrator.migrate('db/mongo/migrate', ENV['VERSION'] ? ENV['VERSION'].to_i : nil)
    end

    namespace :migrate do
      desc 'Rollback MongoDB one migration and re migrate up. If you want to rollback more than one step, define STEP=x. Target specific version with VERSION=x.'
      task :redo do
        if ENV['VERSION']
          Rake::Task['db:mongo:migrate:down'].invoke
          Rake::Task['db:migrate:up'].invoke
        else
          Rake::Task['db:rollback'].invoke
          Rake::Task['db:migrate'].invoke
        end
      end

      desc 'Resets your database using your migrations for the current environment'
      task :reset => %w(db:drop db:create db:migrate)

      desc 'Runs the "up" for a given migration VERSION.'
      task :up do
        version = ENV['VERSION'] ? ENV['VERSION'].to_i : nil
        raise 'VERSION is required' unless version
        Mongo::Migrator.run(:up, 'db/mongo/migrate', version)
      end

      desc 'Runs the "down" for a given migration VERSION.'
      task :down do
        version = ENV['VERSION'] ? ENV['VERSION'].to_i : nil
        raise 'VERSION is required' unless version
        Mongo::Migrator.run(:down, 'db/mongo/migrate', version)
      end
    end

    desc 'Rolls the database back to the previous migration. Specify the number of steps with STEP=n'
    task :rollback do
      step = ENV['STEP'] ? ENV['STEP'].to_i : 1
      Mongo::Migrator.rollback('db/mongo/migrate/', step)
    end

    namespace :schema do
      task :load do
        # todo
      end
    end

    namespace :test do
      task :prepare do
        # todo
      end
    end

  end
end