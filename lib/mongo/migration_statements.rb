module Mongo
  module MigrationStatements
    # http://docs.mongodb.org/manual/reference/command/

    def client
      @client ||= Mongoid::Clients.with_name(:default)
    end

    def use_session(name = :default, &block)
      old_client = @client
      @client = Mongoid::Clients.with_name(name)
      instance_eval(&block) if block
      @client = old_client
    end

    def collection(name)
      if name && Object.const_defined?(name.to_s.classify)
        name.to_s.classify.constantize.collection
      else
        client[name.to_s]
      end
    end

    # Creates Collection with a given name and options.
    # See: http://docs.mongodb.org/manual/reference/command/create/#dbcmd.create
    # OPTIONS:
    # capped: <true|false>
    # autoIndexId: <true|false>
    # size: <max_size>
    # max: <max_documents>
    # flags: <0|1|2|3>
    # Example: create_collection :tracks
    def create_collection(collection_name, options = {})
      client.command({create: collection_name.to_s}.merge(options))
    end

    # The drop command removes an entire collection from a database. see: http://docs.mongodb.org/manual/reference/command/drop/#dbcmd.drop
    # WARNING: This command obtains a write lock on the affected database and will block other operations until it has completed.
    # Example: drop_collection :tracks
    def drop_collection(collection_name)
      client.command(drop: collection_name.to_s)
    end

    # Example: rename_collection :tracks, :songs
    def rename_collection(old_collection_name, new_collection_name)
      collection(old_collection_name).rename(new_collection_name)
    end

    # Sets the value of a field from an entire collection if no criteria is specified.
    # Example: add_field :tracks, :title
    # Example: add_field :tracks, :title, 'Default Song Title'
    def add_field(name, field_name, value = nil)
      collection(name).find(field_name => {'$exists' => false}).update_all('$set' => {field_name.to_s => value})
    end

    # Removes the specified field from an entire collection if no criteria is specified.
    # See: http://docs.mongodb.org/manual/reference/operator/update/unset/#up._S_unset
    # Example: remove_field :tracks, :title
    def remove_field(name, field_name, criteria = {})
      collection(name).find(criteria).update_all('$unset' => {field_name.to_s => ''})
    end

    # Removes the specified field from an entire collection if no criteria is specified.
    # Example: rename_field :tracks, :title, :song_title
    def rename_field(name, old_name, new_name, criteria = {})
      collection(name).find(criteria).update_all('$rename' => {old_name.to_s => new_name.to_s})
    end

    # Example: insert :tracks, <array_of_hashes|hash|>
    def insert(name, data)
      collection(name).insert_one(data)
    end

    # Example: delete :tracks, <array_of_hashes|hash>
    def delete(collection_name, data)
      data.each { |d| connection[collection_name].find(d).remove_all } if data.class == Array
      collection(collection_name).find(data).remove_all if data.class == Hash
    end

    # Example: add_index :tracks
    def add_index(model_name)
      Mongoid::Tasks::Database.create_indexes([model_name.to_s.classify.constantize])
    end

    # Example: remove_index :tracks
    def remove_index(model_name)
      Mongoid::Tasks::Database.remove_indexes([model_name.to_s.classify.constantize])
    end

  end
end
