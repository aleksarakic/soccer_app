class Mongo::Migration
  extend Mongo::MigrationStatements

  @@verbose = true
  cattr_accessor :verbose

  class << self
    def up_with_benchmarks #:nodoc:
      migrate(:up)
    end

    def down_with_benchmarks #:nodoc:
      migrate(:down)
    end

    # Execute this migration in the named direction
    def migrate(direction)
      return unless respond_to?(direction)

      if direction == :up
        announce 'migrating'
      elsif direction == :down
        announce 'reverting'
      end

      result = nil
      time = Benchmark.measure { result = send("#{direction}_without_benchmarks") }

      if direction == :up
        announce format('migrated (%.4fs)', time.real)
        write
      elsif direction == :down
        announce format('reverted (%.4fs)', time.real)
        write
      end

      result
    end

    def singleton_method_added(sym)
      return if defined?(@ignore_new_methods) && @ignore_new_methods

      begin
        @ignore_new_methods = true

        singleton_class.send(:alias_method_chain, sym, 'benchmarks') if sym.in?([:up, :down])
      ensure
        @ignore_new_methods = false
      end
    end

    def write(text = '')
      puts(text) if verbose
    end

    def announce(message)
      version = defined?(@version) ? @version : nil

      text = "#{version} #{name}: #{message}"
      length = [0, 75 - text.length].max
      write format('== %s %s', text, '=' * length)
    end

    def say(message, subitem = false)
      write "#{subitem ? '   ->' : '--'} #{message}"
    end

    def say_with_time(message)
      say(message)
      result = nil
      time = Benchmark.measure { result = yield }
      say format('%.4fs', time.real), :subitem
      say("#{result} rows", :subitem) if result.is_a?(Integer)
      result
    end

    def suppress_messages
      save = verbose
      self.verbose = false
      yield
    ensure
      self.verbose = save
    end

    def connection
      Mongoid.default_client
    end

    def method_missing(method, *arguments, &block)
      arg_list = arguments.map(&:inspect) * ', '
      say_with_time "#{method}(#{arg_list})" do
        connection.send(method, *arguments, &block)
      end
    end
  end
end
